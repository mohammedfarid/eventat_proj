from django.conf.urls import patterns, url,include
from django.conf import settings
from django.conf.urls.static import static
from eventat_web import views

urlpatterns = patterns('',
    #url(r'^$', views.index, name='index'),
    #url(r'^home/$', views.index, name='home'),
    #url(r'^(?P<xx_id>\d+)/$', views.detail, name='detail'),
    # ex: /event_web/
    url(r'^$', views.index, name='index'),
    url(r'^technology/$', views.technology, name='technology'),
    url(r'^seminar/$', views.seminar, name='seminar'),
    url(r'^entertainment/$', views.entertainment, name='entertainment'),
    url(r'^form/$', views.eventform, name='eventform'),
    #url(r'^form/about/$', views.about, name='about'),
    ## ex: /event_web/5/
    #url(r'^(?P<xx_id>\d+)/$', views.detail, name='detail'),
    ## ex: /event_web/5/results/
    #url(r'^(?P<xx_id>\d+)/results/$', views.results, name='results'),
    ## ex: /event_web/5/vote/
    #url(r'^(?P<xx_id>\d+)/vote/$', views.vote, name='vote'),
    

)
if settings.DEBUG:
    urlpatterns += static (settings.STATIC_URL,
                           document_root=settings.STATIC_ROOT)
    urlpatterns += static (settings.MEDIA_URL,
                           document_root=settings.MEDIA_ROOT)