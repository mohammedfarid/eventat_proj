import datetime

from django.db import models
from django.utils import timezone
from PIL import Image

# Create your models here.
class event (models.Model):
    type_event_choise = (
        ('Entertainment','Entertainment'),
        ('Technology','Technology'),
        ('Seminar','Seminar'),

    )
    
    name_event = models.CharField(max_length=250,null=False,default="ABC")
    link_event = models.URLField(max_length=250,null=False,default="http://www.event-at.com")
    details_event = models.TextField(default="ABC")
    type_event = models.CharField(max_length=250,choices=type_event_choise,default='Entertainment')
    image_event = models.ImageField(upload_to="img/",default='img/default.png')#323*565 px
    start_event = models.DateField('start event')
    end_event = models.DateField('end event')
    activate_event = models.BooleanField(default=False)
    #add confermition 
    #pub_date_event = models.DateTimeField('date published')
    
    timestamp = models.DateTimeField(auto_now_add=True,auto_now=False)
    update = models.DateTimeField(auto_now_add=False,auto_now=True)
    def __unicode__(self):
        return self.name_event
    #def was_published_recently(self):
    #    return self.pub_date_event >= timezone.now() - datetime.timedelta(days=1)
    #was_published_recently.admin_order_field = 'pub_date_event'
    #was_published_recently.boolean = True
    #was_published_recently.short_description = 'Published recently?'
    def save(self):
        if not self.image_event:
            return            
        super(event, self).save()
        image = Image.open(self.image_event.path)
        (width, height) = image.size     
        size = (323,565)
        image = image.resize(size, Image.ANTIALIAS)
        image.save(self.image_event.path)