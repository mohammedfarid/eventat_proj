from django.shortcuts import render
from eventat_web.models import event
from django.template import RequestContext, loader
from django.shortcuts import render_to_response
from django.http import Http404


from django.http import HttpResponse
from .models import event
from .form import EventForm
# Create your views here

def index(request):
    #homa Page
    context = RequestContext(request)
    event_list = event.objects.all()
    context_dict = {'events': event_list}
    #context_dict['eventat'] = event_list
    return render_to_response('eventat_web/index.html', context_dict, context)

def technology(request):
    #homa Page
    context = RequestContext(request)
    event_list = event.objects.all()
    context_dict = {'events': event_list}
    #context_dict['eventat'] = event_list
    return render_to_response('eventat_web/technology.html', context_dict, context)

def seminar(request):
    #homa Page
    context = RequestContext(request)
    event_list = event.objects.all()
    context_dict = {'events': event_list}
    #context_dict['eventat'] = event_list
    return render_to_response('eventat_web/seminar.html', context_dict, context)

def entertainment(request):
    #homa Page
    context = RequestContext(request)
    event_list = event.objects.all()
    context_dict = {'events': event_list}
    #context_dict['eventat'] = event_list
    return render_to_response('eventat_web/entertainment.html', context_dict, context)


def eventform (request):
    #form Page
    if request.method == 'POST':
        form = EventForm(request.POST,request.FILES)
        context = {'form':form}
        #print form
        if form.is_valid():
            save_it = form.save(commit=False)
            #name_event=form.cleaned_data['name_event']
            #image_event=form.cleaned_data['image_event']
            #save_it = event(image_event= request.FILES['img'])
            #type_event=form.cleaned_data['type_event']
            #link_event=form.cleaned_data['link_event']
            #details_event=form.cleaned_data['details_event']
            #start_event=form.cleaned_data['start_event']
            #end_event=form.cleaned_data['end_event']
            save_it.save()
            return render(request,'eventat_web/add-event-confirmation.html',context)
    else:
        form = EventForm()
    return render(request,'eventat_web/form.html',{'form': form})


#def detail(request, xx_id):
#    return HttpResponse("You're looking at question %s." % xx_id)
#def results(request, xx_id):
#    response = "You're looking at the results of question %s."
#    return HttpResponse(response % xx_id)
#
#def vote(request, xx_id):
#    return HttpResponse("You're voting on question %s." % xx_id)



#def about (request):
#    context = {}
#    return render(request,'eventat_web/index.html',  context)