# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eventat_web', '0029_auto_20141019_0304'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='image_event',
            field=models.ImageField(default=b'img/default.png', upload_to=b'img/'),
        ),
        migrations.AlterField(
            model_name='event',
            name='type_event',
            field=models.CharField(default=b'Entertainment', max_length=250, choices=[(b'Entertainment', b'Entertainment'), (b'Technology', b'Technology'), (b'Seminar', b'Seminar')]),
        ),
    ]
