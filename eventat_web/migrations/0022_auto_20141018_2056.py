# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eventat_web', '0021_event_image_event'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='event',
            name='image_event',
        ),
        migrations.AlterField(
            model_name='event',
            name='details_event',
            field=models.TextField(default=b'ABC'),
        ),
        migrations.AlterField(
            model_name='event',
            name='link_event',
            field=models.URLField(default=b'www.event-at.com', max_length=250),
        ),
        migrations.AlterField(
            model_name='event',
            name='name_event',
            field=models.CharField(default=b'ABC', max_length=250),
        ),
    ]
