# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eventat_web', '0010_remove_event_pub_date_event'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='image_event_height',
            field=models.IntegerField(default=b'565', null=True, editable=False, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='image_event_width',
            field=models.IntegerField(default=b'323', null=True, editable=False, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='image_event',
            field=models.ImageField(upload_to=b'img/'),
        ),
    ]
