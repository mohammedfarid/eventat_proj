# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eventat_web', '0014_auto_20141018_0236'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='event',
            name='details_event',
        ),
        migrations.RemoveField(
            model_name='event',
            name='end_event',
        ),
        migrations.RemoveField(
            model_name='event',
            name='image_event',
        ),
        migrations.RemoveField(
            model_name='event',
            name='image_event_height',
        ),
        migrations.RemoveField(
            model_name='event',
            name='image_event_width',
        ),
        migrations.RemoveField(
            model_name='event',
            name='link_event',
        ),
        migrations.RemoveField(
            model_name='event',
            name='start_event',
        ),
        migrations.RemoveField(
            model_name='event',
            name='type_event',
        ),
    ]
