# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('eventat_web', '0007_auto_20141017_1924'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='details_event',
            field=models.TextField(default=datetime.date(2014, 10, 17)),
            preserve_default=False,
        ),
    ]
