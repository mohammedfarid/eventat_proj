# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eventat_web', '0004_event_pub_date_event'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='link_event',
            field=models.URLField(max_length=250),
        ),
    ]
