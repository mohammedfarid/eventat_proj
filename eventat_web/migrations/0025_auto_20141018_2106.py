# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eventat_web', '0024_event_name_event'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='details_event',
            field=models.TextField(default=b'ABC'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='link_event',
            field=models.URLField(default=b'http://www.event-at.com', max_length=250),
            preserve_default=True,
        ),
    ]
