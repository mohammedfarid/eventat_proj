# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('eventat_web', '0022_auto_20141018_2056'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='event',
            name='details_event',
        ),
        migrations.RemoveField(
            model_name='event',
            name='end_event',
        ),
        migrations.RemoveField(
            model_name='event',
            name='link_event',
        ),
        migrations.RemoveField(
            model_name='event',
            name='name_event',
        ),
        migrations.RemoveField(
            model_name='event',
            name='start_event',
        ),
        migrations.RemoveField(
            model_name='event',
            name='type_event',
        ),
        migrations.AddField(
            model_name='event',
            name='image_event',
            field=models.ImageField(default=datetime.date(2014, 10, 18), upload_to=b'img/'),
            preserve_default=False,
        ),
    ]
