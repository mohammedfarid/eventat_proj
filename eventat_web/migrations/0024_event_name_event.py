# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eventat_web', '0023_auto_20141018_2103'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='name_event',
            field=models.CharField(default=b'ABC', max_length=250),
            preserve_default=True,
        ),
    ]
