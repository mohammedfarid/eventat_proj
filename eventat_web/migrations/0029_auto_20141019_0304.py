# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eventat_web', '0028_event_activate_event'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='activate_event',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='event',
            name='type_event',
            field=models.CharField(default=b'Entertainment', max_length=250, choices=[(b'Entertainment', b'Entertainment'), (b'Technology', b'Technology'), (b'Seminar', b'Seminar'), (b'Conference', b'Conference')]),
        ),
    ]
