# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eventat_web', '0019_auto_20141018_0436'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='type_event',
            field=models.CharField(default=b'mi', max_length=2, choices=[(b'mi', b'ali'), (b'mi', b'ali')]),
            preserve_default=True,
        ),
    ]
