# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eventat_web', '0012_auto_20141017_2105'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='end_event',
            field=models.DateField(verbose_name=b'end event'),
        ),
        migrations.AlterField(
            model_name='event',
            name='start_event',
            field=models.DateField(verbose_name=b'start event'),
        ),
    ]
