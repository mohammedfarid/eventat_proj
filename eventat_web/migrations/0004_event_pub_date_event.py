# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('eventat_web', '0003_event_link_event'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='pub_date_event',
            field=models.DateTimeField(default=datetime.date(2014, 9, 30), verbose_name=b'date published'),
            preserve_default=False,
        ),
    ]
