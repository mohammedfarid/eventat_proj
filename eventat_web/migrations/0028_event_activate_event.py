# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eventat_web', '0027_auto_20141018_2110'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='activate_event',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
    ]
