# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('eventat_web', '0002_auto_20140930_1752'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='link_event',
            field=models.CharField(default=datetime.date(2014, 9, 30), max_length=250),
            preserve_default=False,
        ),
    ]
