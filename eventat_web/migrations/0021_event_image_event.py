# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('eventat_web', '0020_event_type_event'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='image_event',
            field=models.ImageField(default=datetime.date(2014, 10, 18), upload_to=b'img/'),
            preserve_default=False,
        ),
    ]
