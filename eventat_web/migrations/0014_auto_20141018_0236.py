# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eventat_web', '0013_auto_20141017_2106'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='image_event_height',
            field=models.IntegerField(default=b'323', null=True, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='event',
            name='image_event_width',
            field=models.IntegerField(default=b'565', null=True, editable=False, blank=True),
        ),
    ]
