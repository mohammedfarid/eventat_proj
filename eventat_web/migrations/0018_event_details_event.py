# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('eventat_web', '0017_auto_20141018_0430'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='details_event',
            field=models.TextField(default=datetime.date(2014, 10, 18)),
            preserve_default=False,
        ),
    ]
