# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eventat_web', '0011_auto_20141017_2102'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='image_event',
            field=models.ImageField(height_field=b'image_event_height', width_field=b'image_event_width', upload_to=b'img/'),
        ),
    ]
