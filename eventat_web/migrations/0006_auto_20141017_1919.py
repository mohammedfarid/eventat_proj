# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('eventat_web', '0005_auto_20141001_1459'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='timestamp',
            field=models.DateTimeField(default=datetime.date(2014, 10, 17), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='event',
            name='update',
            field=models.DateTimeField(default=datetime.date(2014, 10, 17), auto_now=True),
            preserve_default=False,
        ),
    ]
