# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('eventat_web', '0008_event_details_event'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='image_event',
            field=models.ImageField(default=datetime.date(2014, 10, 17), upload_to=b'/static/img/'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='event',
            name='type_event',
            field=models.CharField(default=b'mi', max_length=2, choices=[(b'mi', b'ali'), (b'mi', b'ali')]),
            preserve_default=True,
        ),
    ]
