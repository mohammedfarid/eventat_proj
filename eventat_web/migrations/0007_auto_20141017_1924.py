# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('eventat_web', '0006_auto_20141017_1919'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='end_event',
            field=models.DateField(default=datetime.date(2014, 10, 17), verbose_name=b'end_event'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='event',
            name='start_event',
            field=models.DateField(default=datetime.date(2014, 10, 17), verbose_name=b'start_event'),
            preserve_default=False,
        ),
    ]
