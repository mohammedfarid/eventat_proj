# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('eventat_web', '0016_event_image_event'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='event',
            name='image_event',
        ),
        migrations.AddField(
            model_name='event',
            name='link_event',
            field=models.URLField(default=datetime.date(2014, 10, 18), max_length=250),
            preserve_default=False,
        ),
    ]
