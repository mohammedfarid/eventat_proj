from django import forms

from .models import event

class EventForm(forms.ModelForm):
    #image_event = forms.ImageField(upload_to="img/", help_text="upload image" , required=True )
    class Meta:
        model=event
  
  
    def clean(self):
        cleaned_data = self.cleaned_data
        url = cleaned_data.get('link_event')
        # If url is not empty and doesn't start with 'http://', prepend 'http://'.
        if url and not url.startswith('http://'):
            url = 'http://' + url
            cleaned_data['link_event'] = url

        return cleaned_data

    
