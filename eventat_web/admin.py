from django.contrib import admin

#import model
from eventat_web.models import event

class EventAdmin(admin.ModelAdmin):
  fieldsets = [
        #(None, {'fields':['image_event']})
        (None,               {'fields': ['name_event']}),
        ('Date information', {'fields': ['start_event','end_event'],'classes': ['collapse']}),
        ('Data Event',{'fields':['link_event','details_event','image_event'],'classes': ['collapse']}),
        ('Category Event',{'fields':['type_event'],'classes': ['collapse']}),
        ('Confirmation Event',{'fields':['activate_event']})
    ]
  list_display = ('name_event', 'start_event','end_event', 'link_event','activate_event')
  list_filter = ['start_event','link_event','activate_event']
  search_fields = ['name_event','link_event']
# Register your models here.
admin.site.register(event,EventAdmin)