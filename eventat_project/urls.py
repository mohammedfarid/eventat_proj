from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    #url(r'^$', 'eventat_project.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^', include('eventat_web.urls')),
    url(r'^adminsetting/', include(admin.site.urls)),
)
